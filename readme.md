# zk-v3-cli

## 一个基于`Vue3`+`Vite3` +`TS`的脚手架工具

> 目前只支持Vue，后续会考虑其他框架

快速安装

```tsx
npm install zk-v3-cli -g
```

### 项目创建

```tsx
zk create <projectName>
```

项目已经帮你配置以下模块：

- 常用的目录结构
- 初始化css样式
- 二次封装axios以及自定义返回值类型
- vue-router的安装和配置
- pinia的安装和配置
- 环境变量配置
- 路径别名配置
- 样式适配px2rem
- 全局引入less混合
- 代理请求
- 自动引入vue、vue-router的hook和element-plus的组件
- 兼容传统浏览器

### 项目开发

项目开发目前提供四个功能：

1. 创建Vue组件
2. 创建Vue页面
3. 创建Pinia仓库
4. 创建接口文件及其对应的类型文件

#### 创建Vue组件

```tsx
zk addcpt <cptName> // 例如 zk addcpt NavBar 默认会放到src/components文件夹中
zk addcpt <cptName> -d src/pages // 也可以通过-d来指定要存放的文件夹
```

#### 创建Vue页面

```tsx
zk addpage <pageName> // 例如 zk addpage NavBar 默认会放到src/pages文件夹中
zk addpage <pageName> -d src/pages/User/Post // 也可以通过-d来指定要存放的文件夹
```

#### 创建store仓库

```tsx
zk addstore <storeName> // 例如zk addstore cart 会放到src/store文件夹中
```

#### 创建接口文件及其对应的类型文件

```tsx
zk addreq <reqName> // 例如zk addreq user 接口文件会放到src/http中，对应的类型文件会放到src/http/types中
```
