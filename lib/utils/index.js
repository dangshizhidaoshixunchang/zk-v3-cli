const ejs = require('ejs')
const path = require('path')

const complieEjs = async (ejsName, data) => {
    return new Promise((resolve, reject) => {
        // renderFile必须是绝对路径
        ejs.renderFile(path.resolve(__dirname, `../template/${ejsName}.ejs`), { data },  (err, str) => {
            if(err) {
                reject(err)
            }else {
                resolve(str)
            }
        })
    })
}

module.exports = {
    complieEjs
}