const { promisify } = require('util')
const config = require('../config')
const chalk = require('chalk')
const ora = require('ora')
const fs = require('fs')
const download = promisify(require('download-git-repo'))
const { complieEjs } = require('../utils')


// 创建项目
exports.createProject = async (projectName) => {
    const spinner = ora().start();
    spinner.text = '正在创建项目...'
    await download(config.downloadUrl, `./${projectName}`, { clone: true })
	spinner.succeed('项目创建完成')
    console.log(chalk.greenBright.bold('Done!!'), chalk.gray.bold('you can'))
    console.log('cd', projectName)
    console.log('npm install')
    console.log('npm run dev')
}

// 创建页面
exports.addPage = async (pageName, rootPath) => {
    const pathArr = rootPath.split('/')
    // 判断用户给的路径是否存在，如果不存在则先创建目录后再创建页面
    pathArr.reduce((prev, item) => {
        if(!fs.existsSync(`${prev}/${item}`)) {
            fs.mkdirSync(`${prev}/${item}`)
        }
        return prev += `/${item}`
    }, './')
    const compileRet = await complieEjs('vuePage', {
        name: pageName,
        lowerName: pageName.charAt(0).toLowerCase() + pageName.slice(1)
    })
    fs.mkdirSync(`${rootPath}/${pageName}`)
    await fs.promises.writeFile(`${rootPath}/${pageName}/${pageName}.vue`, compileRet)
    console.log(chalk.green.bold(`${pageName}页面创建成功`))
}

// 创建组件
exports.addCpt = async (cptName, rootPath) => {
    fs.mkdirSync(`${rootPath}/${cptName}`)
    const compileRet = await complieEjs('vuePage', {
        name: cptName,
        lowerName: cptName.charAt(0).toLowerCase() + cptName.slice(1)
    })
    await fs.promises.writeFile(`${rootPath}/${cptName}/${cptName}.vue`, compileRet)
    console.log(chalk.green.bold(`${cptName}组件创建成功`))
}

// 创建store仓库
exports.addStore = async (storeName, rootPath) => { 
    const compileRet = await complieEjs('useStore', {
        name: storeName,
        upperName: storeName.charAt(0).toUpperCase() + storeName.slice(1)
    })
    await fs.promises.writeFile(`${rootPath}/${storeName}.ts`, compileRet)
    console.log(chalk.green.bold(`${storeName}仓库创建成功`))
}

// 创建请求文件
exports.addReq = async (reqName) => {
    const compileReq = await complieEjs('request', { name: reqName })
    const compileReqType = await complieEjs('requestType', { 
        upperName: reqName.charAt(0).toUpperCase() + reqName.slice(1)
    })
    await fs.promises.writeFile(`src/http/${reqName}.ts`, compileReq)
    await fs.promises.writeFile(`src/http/types/${reqName}Type.ts`, compileReqType)
    console.log(chalk.green.bold(`${reqName}请求创建成功`))
}