const program = require('commander')
const action = require('./action')

module.exports = () => {
    // 创建项目
    program
        .command('create <projectName>')
        .description('create a project')
        .action(action.createProject)
    
    // 创建页面
    program
        .command('addpage <pageName>')
        .description('add a page')
        .action((pageName) => action.addPage(pageName, program.dest || 'src/pages'))

    // 创建组件
    program
        .command('addcpt <cptName>')
        .description('add a component')
        .action((cptName) => action.addCpt(cptName, program.dest || 'src/components'))

    // 创建store仓库
    program
        .command('addstore <storeName>')
        .description('add a store')
        .action((storeName) => action.addStore(storeName, program.dest || 'src/store'))

    // 创建请求文件
    program
        .command('addreq <reqName>')
        .description('add a req')
        .action(action.addReq)
}

