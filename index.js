#! /usr/bin/env node

const program = require('commander')
const coreCommand = require('./lib/core')

program.option('-d --dest <dest>', 'whitch dest do you want to create')
program.version(require('./package.json').version)
coreCommand()
program.parse(process.argv)